import App, { Container } from "next/app";
import Router from "next/router";
import React from "react";
import LangProvider from "~/providers/LangProvider";

Router.events.on("routeChangeComplete", () => {
    window.scrollTo(0, 0);
});

export default class extends App {
    static async getInitialProps({ ctx, Component }) {
        // manually call getInitialProps on child component (just in case the page component is using it)
        const pageProps = Component.getInitialProps
            ? await Component.getInitialProps(ctx)
            : {};

        const langProps = await LangProvider.getInitialProps(ctx);

        return { pageProps, langProps };
    }

    constructor(props) {
        super(props);
    }

    componentDidCatch(error, errorInfo) {
        Sentry.configureScope(scope => {
            Object.keys(errorInfo).forEach(key => {
                scope.setExtra(key, errorInfo[key]);
            });
        });
        Sentry.captureException(error);

        // This is needed to render errors correctly in development / production
        super.componentDidCatch(error, errorInfo);
    }

    render() {
        const { Component, pageProps, langProps } = this.props;

        return (
            <Container>
                <LangProvider {...langProps}>
                    <Component {...pageProps} />
                </LangProvider>
            </Container>
        );
    }
}
