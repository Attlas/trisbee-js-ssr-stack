import * as React from "react";
import { I18nProvider } from "@lingui/react";
import catalogCs from "~/locales/cs/messages";
import catalogEn from "~/locales/en/messages";
import { supportedLangs } from "~/../langUtils";

const catalogs = { cs: catalogCs, en: catalogEn };

// Util function to decide language based on browser prefferences
const decideLangFromPref = (req) => {
    const locale = require("locale");

    const sampleLangs = new locale.Locales(supportedLangs, "en");
    const prefLangs = new locale.Locales(req.headers["accept-language"]);
    return prefLangs.best(sampleLangs).toString();
};

const LangContext = React.createContext({
    state: {
        lang: "en"
    }
});

const LangProvider = (props) => {
    return (
        <I18nProvider language={props.clientLang} catalogs={catalogs}>
            <LangContext.Provider
                value={{
                    state: {
                        lang: props.clientLang
                    }
                }}
            >
                {props.children}
            </LangContext.Provider>
        </I18nProvider>
    );
};

LangProvider.getInitialProps = async ({ req, res, asPath }) => {
    // Decides a language
    if (req && (!req.url.startsWith("/en") && !req.url.startsWith("/cs"))) {
        let lang;
        if (req.headers.cookie) {
            if (req.cookies.lang && supportedLangs.includes(req.cookies.lang)) {
                lang = req.cookies.lang;
            } else {
                lang = decideLangFromPref(req);
            }
        } else {
            lang = decideLangFromPref(req);
        }

        // Redirects
        if (!res.headersSent) {
            res.writeHead(302, {
                Location: "/" + lang + req.url
            });
            res.end();
        }
        return;
    }

    let clientLang = asPath.substr(1, 2);
    if (!supportedLangs.includes(clientLang)) {
        clientLang = "en";
    }

    return { clientLang };
};

export { LangContext};
export default LangProvider;
