import React from 'react';
import "./style.scss";
import {Trans} from "@lingui/macro";

const Home = () => {
    return (
        <div className={"home"}>
            <Trans id={"foo"} />
        </div>
    );
};

export default Home;