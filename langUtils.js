// langUtils.js is in root, because it is both used by next server and client app

const supportedLangs = ["cs", "en"];

const getSuppLangsReFragment = langs => {
  const allLangs = langs.reduce((acc, e, i) => {
    if (i + 1 === langs.length) {
      return acc + e;
    } else {
      return acc + e + "|";
    }
  }, "");

  return "(" + allLangs + ")";
};

module.exports.supportedLangs = supportedLangs;
module.exports.getSuppLangsReFragment = getSuppLangsReFragment;
