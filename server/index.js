// This file doesn't go through babel or webpack transformation.
// Make sure the syntax and sources this file requires are compatible with the current node version you are running
// See https://github.com/zeit/next.js/issues/1245 for discussions on Universal Webpack or universal Babel

const express = require("express");
const { parse } = require("url");
const next = require("next");
const cookieParser = require("cookie-parser");
const langUtils = require("../langUtils");

const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });

const handle = app.getRequestHandler();

app.prepare().then(() => {
  const server = express();

  server.use(cookieParser());

  // Creates a param for marching language
  const suppLangsReFragment = langUtils.getSuppLangsReFragment(
    langUtils.supportedLangs
  );
  const langParam = "/:lang(" + suppLangsReFragment + ")";

  // Default language route that strips request from it's language and renders desired page
  server.get(langParam + "*", (req, res) => {
    // Be sure to pass `true` as the second argument to `url.parse`.
    // This tells it to parse the query portion of the URL.
    const parsedUrl = parse(req.url, true);
    const { pathname, query } = parsedUrl;

    // Always render a template without /:lang
    app.render(req, res, req.url.substr(3) || "/", {
      lang: req.params.lang,
      ...query
    });
  });

  server.get("*", (req, res) => {
    handle(req, res);
  });

  server.listen(process.env.PORT || 3000);
});
